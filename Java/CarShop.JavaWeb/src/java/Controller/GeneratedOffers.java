/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controller;

import Model.Offer;
import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Pasa
 */
@XmlRootElement()
public class GeneratedOffers {
    @XmlElement
    List<Offer> offers;
    
    public GeneratedOffers() {
        
    }
    public GeneratedOffers(int numberOfOffers, String carName, int price) {
        
       this.offers = GenerateOffers(numberOfOffers, carName, price);
    }
    
    
    private List<Offer> GenerateOffers(int numberOfOffers, String carName, int price)
    {
        ArrayList<Offer> temp = new ArrayList<>();
        for (int i = 0; i < numberOfOffers; i++) 
        {
            temp.add(new Offer(carName,price ));
            
        }
        return temp;
    }
}
