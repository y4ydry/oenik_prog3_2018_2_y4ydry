/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model;

import java.util.Random;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Pasa
 */
@XmlRootElement
public class Offer {
    
    private String carName;
    private String buyerName;
    private int price;
    
        public Offer() {
            
    }

    public Offer(String carName, int price) {
        Random rand = new Random();
        this.carName = carName;
        this.buyerName = "buyer" + rand.nextInt(100);
        this.price = price + price *  (rand.nextInt(20)-10)/20;
    }
    @XmlElement(name = "carname")
    public String getCarName() {
        return carName;
    }
    @XmlElement(name = "buyerName")
    public String getBuyerName() {
        return buyerName;
    }
    @XmlElement(name = "price")
    public int getPrice() {
        return price;
    }
    
}
