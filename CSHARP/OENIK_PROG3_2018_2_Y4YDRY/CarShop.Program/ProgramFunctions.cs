﻿// <copyright file="ProgramFunctions.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace CarShop.Program
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Text;
    using System.Threading.Tasks;
    using System.Xml.Linq;
    using CarShop.CarShopLogic;
    using CarShop.Data;
    using CarShop.Repo;

    /// <summary>
    /// Class of entry point, and program functions
    /// </summary>
    internal partial class Program
    {
        /// <summary>
        /// Handles modifying records in tables. Asks for inputs, then calls the function, for modifying from the logic class. It decides which table to act upon, based on the values in the parameters parameter
        /// </summary>
        /// <param name="parameters">Contains parameters, that are stored in the MenuElem class</param>
        public static void ModifyTable(string[] parameters)
        {
            int id = 0;
            string columnName = " ";
            string value = " ";
            Console.WriteLine("Adja meg a módosítani kivánt tábla id-jét(1-tol szamozva)");
            id = int.Parse(Console.ReadLine());

            Console.WriteLine("Válaszon melyik oszlopot szeretne módosítani");

            PropertyInfo[] props;

            switch (parameters[0])
            {
                case "automarkak":
                    props = typeof(AUTOMARKAK).GetProperties();
                    break;

                case "modellek":
                    props = typeof(MODELLEK).GetProperties();
                    break;
                case "extrak":
                    props = typeof(EXTRAK).GetProperties();
                    break;

                case "model extra kapcsolo":
                    props = typeof(MODELL_EXTRA_KAPCSOLO).GetProperties();
                    break;
                default:
                    props = typeof(AUTOMARKAK).GetProperties();
                    break;
            }

            for (int i = 0; i < props.Length; i++)
            {
                if (props[i].GetGetMethod().IsVirtual == false)
                {
                    Console.WriteLine(i + ". " + props[i].Name);
                }
            }

            columnName = props[int.Parse(Console.ReadLine())].Name;

            Console.WriteLine("Írja be az uj erteket");

            value = Console.ReadLine();

            switch (parameters[0])
            {
                case "automarkak":
                    Logic.ModifyBrandById(id, columnName, value);
                    break;

                case "modellek":
                    Logic.ModifyModellById(id, columnName, value);
                    break;
                case "extrak":
                    Logic.ModifyExtraById(id, columnName, value);
                    break;

                case "model extra kapcsolo":
                    Console.WriteLine("ezt a talbat nem lehet modositani");
                    break;
                default:
                    break;
            }
        }

        /// <summary>
        /// Handles listing records from tables. It decides which table to act upon, based on the values in the parameters parameter
        /// </summary>
        /// <param name="parameters">Contains parameters, that are stored in the MenuElem class</param>
        public static void TableList(string[] parameters)
        {
            string tableName = parameters[0];

            switch (tableName)
            {
                case "automarkak":

                    var autoMarkak = Logic.ReadBrands();
                    Console.WriteLine("id" + "\t\t" + "NEV" + "\t\t" + "ORSZAGNEV" + "\t\t" + "EVES_FORGALOM" + "\t\t" + "ALAPITAS_EVE" + "\t\t" + "C_URL\n");
                    foreach (var item in autoMarkak)
                    {
                        Console.WriteLine(item.ID + "\t\t" + item.NEV + "\t\t" + item.ORSZAGNEV + "\t\t" + item.EVES_FORGALOM + "\t\t" + item.ALAPITAS_EVE + "\t\t" + item.C_URL);
                    }

                    break;

                case "modellek":

                    var modellek = Logic.ReadModels();
                    Console.WriteLine("ID" + "\t\t" + "Nev" + "\t\t" + "Loero" + "\t\t" + "Megjelenes Eve" + "\t\t" + "Motor terfogat" + "\t\t" + "Marka Id" + "\t\t" + "Alapár\n");
                    foreach (var item in modellek)
                    {
                        Console.WriteLine(item.ID + "\t\t" + item.NEV + "\t\t" + item.LOERO + "\t\t" + item.MEGJELENES_EVE + "\t\t" + item.MOTOR_TERFOGAT + "\t\t" + item.MARKA_ID + "\t\t" + item.ALAPAR);
                    }

                    break;
                case "extrak":

                    var extrak = Logic.ReadExtras();

                    Console.WriteLine("ID" + "\t\t" + "KATEGORIA_NEV" + "\t\t" + "NEV" + "\t\t" + "AR" + "\t\t" + "IGENYTELEN_E" + "\t\t" + "SZIN\n");

                    foreach (var item in extrak)
                    {
                        Console.WriteLine(item.ID + "\t\t" + item.KATEGORIA_NEV + "\t\t" + item.NEV + "\t\t" + item.AR + "\t\t" + item.IGENYTELEN_E + "\t\t" + item.SZIN);
                    }

                    break;

                case "model extra kapcsolo":
                    var kapcsolo = Logic.ReadExtraConnections();

                    Console.WriteLine("ID" + "\t\t" + "EXTRA_ID" + "\t\t" + "MODELL_ID");
                    foreach (var item in kapcsolo)
                    {
                        Console.WriteLine(item.ID + "\t\t" + item.EXTRA_ID + "\t\t" + item.MODELL_ID);
                    }

                    break;
                default:
                    break;
            }
        }

        /// <summary>
        /// Handles inserting into tables. Asks for inputs, then calls the function, for inserting from the logic class. It decides which table to act upon, based on the values in the parameters parameter
        /// </summary>
        /// <param name="parameters">Contains parameters, that are stored in the MenuElem class</param>
        public static void InsertIntoTable(string[] parameters)
        {
            switch (parameters[0])
            {
                case "automarkak":

                    Console.Write("id(szam): ");
                    decimal a_id = decimal.Parse(Console.ReadLine());

                    Console.Write("nev: ");
                    string a_nev = Console.ReadLine();

                    Console.Write("alapitasEve(szam): ");
                    decimal a_alapitasEve = decimal.Parse(Console.ReadLine());

                    Console.Write("url: ");
                    string a_url = Console.ReadLine();

                    Console.Write("evesForgalom(szam): ");
                    decimal a_evesForgalom = decimal.Parse(Console.ReadLine());

                    Console.Write("orszagnev: ");
                    string orszagnev = Console.ReadLine();

                    Logic.InsertBrand(a_id, a_nev, a_alapitasEve, a_url, a_evesForgalom, orszagnev);
                    break;

                case "modellek":
                    Console.Write("id(szam): ");
                    decimal m_id = decimal.Parse(Console.ReadLine());

                    Console.Write("markaId(szam)(markanak leteznie kell): ");
                    decimal markaId = decimal.Parse(Console.ReadLine());

                    Console.Write("Nev: ");
                    string m_nev = Console.ReadLine();

                    Console.Write("megjelenesEve(szam): ");
                    decimal megjelenesEve = decimal.Parse(Console.ReadLine());

                    Console.Write("motorTerfogat(szam): ");
                    decimal motorTerfogat = decimal.Parse(Console.ReadLine());

                    Console.Write("loero(szam): ");
                    decimal loero = decimal.Parse(Console.ReadLine());

                    Console.Write("alapar(szam): ");
                    decimal alapar = decimal.Parse(Console.ReadLine());

                    Logic.InsertModel(m_id, markaId, m_nev, megjelenesEve, motorTerfogat, loero, alapar);

                    break;
                case "extrak":
                    Console.Write("id(szam): ");
                    decimal e_id = decimal.Parse(Console.ReadLine());

                    Console.Write("kategoriaNev: ");
                    string e_kategoriaNev = Console.ReadLine();

                    Console.Write("nev: ");
                    string e_nev = Console.ReadLine();

                    Console.Write("ar(szam): ");
                    decimal e_ar = decimal.Parse(Console.ReadLine());

                    Console.Write("szin: ");
                    string e_szin = Console.ReadLine();

                    Console.Write("IgenytelenE(true/false): ");
                    string temp = Console.ReadLine();

                    bool e_igenytelenE;
                    if (temp == "true")
                    {
                        e_igenytelenE = true;
                    }
                    else
                    {
                        e_igenytelenE = false;
                    }

                    Logic.InsertExtra(e_id, e_kategoriaNev, e_nev, e_ar, e_szin, e_igenytelenE);
                    break;

                case "model extra kapcsolo":
                    Console.Write("id(szam): ");
                    decimal mk_id = decimal.Parse(Console.ReadLine());

                    Console.Write("modelId(szam)(modelnek leteznie kell): ");
                    decimal mk_modelId = decimal.Parse(Console.ReadLine());

                    Console.Write("extraId(szam)(extranak leteznie kell): ");
                    decimal mk_extraId = decimal.Parse(Console.ReadLine());

                    Logic.InsertExtraConnection(mk_id, mk_modelId, mk_extraId);
                    break;
                default:
                    break;
            }
        }

        /// <summary>
        /// Handles removing records. Asks for inputs, then calls the function, for removing from the logic class. It decides which table to act upon, based on the values in the parameters parameter
        /// </summary>
        /// <param name="parameters">Contains parameters, that are stored in the MenuElem class</param>
        public static void RemoveFromTableById(string[] parameters)
        {
            Console.WriteLine("Adja meg a torolni kivant rekord idjet");
            decimal id = decimal.Parse(Console.ReadLine());

            switch (parameters[0])
            {
                case "automarkak":
                    Logic.RemoveBrandById(id);
                    break;

                case "modellek":
                    Logic.RemoveModellById(id);
                    break;
                case "extrak":
                    Logic.RemoveExtraById(id);
                    break;

                case "model extra kapcsolo":
                    Logic.RemoveExtraConnectionById(id);
                    break;
                default:
                    break;
            }
        }

        /// <summary>
        /// Reads input from user, writes the average cost of a brand to the console
        /// </summary>
        public static void AverageCostOfBrand()
        {
            Console.WriteLine("Adja meg a keresett marka idjet");
            int id = int.Parse(Console.ReadLine());

            decimal ertek = Logic.AverageCostOfBrand(id);

            Console.WriteLine("A megadott marka modellek árának átlaga:" + ertek + "ft");
        }

        /// <summary>
        /// Writes number of ugly extras to the console
        /// </summary>
        public static void NumberOfUglyExtras()
        {
            decimal ertek = Logic.NumberOfUglyExtras();
            Console.WriteLine("Osszesen " + ertek + " darab igenytelen extra van");
        }

        /// <summary>
        /// Writes the name of the most expensive model to the consol
        /// </summary>
        public static void NameOfMostExpensiveModel()
        {
            string ertek = Logic.NameOfMostExpensiveModel();

            Console.WriteLine("Legdragabb model neve: " + ertek);
        }

        /// <summary>
        /// Takes offer from the java servlet
        /// </summary>
        public static void ListOffers()
        {
            Console.WriteLine("Adja meg egy kocsi nevet");
            string carName = Console.ReadLine();

            Console.WriteLine("Adjon meg egy iranyado árat");
            string price = Console.ReadLine();

            XDocument xdoc = XDocument.Load(@"http://localhost:8080/CarShop.JavaWeb/offers?buyername=" + carName + "&price=" + price);

            Console.WriteLine("Vevő név" + "\t" + "Kocsi név" + "\t" + "Ár");
            foreach (var item in xdoc.Element("generatedOffers").Elements("offers"))
            {
                Console.WriteLine(item.Element("buyerName").Value + "\t\t" + item.Element("carname").Value + "\t\t" + item.Element("price").Value);
            }
        }
    }
}
