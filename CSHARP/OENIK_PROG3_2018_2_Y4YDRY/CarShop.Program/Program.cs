﻿// <copyright file="Program.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace CarShop.Program
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using CarShop.CarShopLogic;
    using CarShop.Repo;

    /// <summary>
    /// Class of entry point, and program functions
    /// </summary>
    internal partial class Program
    {
        /// <summary>
        /// Gets or sets logic instance for the whole program
        /// </summary>
        public static Logic Logic { get; set; }

        private static void Main()
        {
            while (true)
            {
                Menu();
            }
        }

        private static void Menu()
        {
            // sets up the structure of the menu and creates logic variable
            Logic = new Logic(new Repository());
            MenuElem gyoker = new MenuElem("gyoker", true);

            List<MenuElem> elsoSzint = new List<MenuElem>()
            {
                new MenuElem("Tabla Funkciok"),
                new MenuElem("Ajanlat lekeres", (x) => ListOffers())
            };

            List<MenuElem> tablaFunkciok = new List<MenuElem>()
            {
                new MenuElem("Crud"),
                new MenuElem("Lekerdezesek")
            };

            List<MenuElem> tablak = new List<MenuElem>()
            {
                new MenuElem("automarkak",  "automarkak"),
                new MenuElem("modellek", "modellek"),
                new MenuElem("extrak", "extrak"),
                new MenuElem("Model extra kapcsolo", "model extra kapcsolo")
            };

            List<MenuElem> muveletCollection = new List<MenuElem>()
            {
                new MenuElem("listazas", (x) => TableList(x)),
                new MenuElem("beszuras", (x) => InsertIntoTable(x)),
                new MenuElem("modositas", (x) => ModifyTable(x)),
                new MenuElem("torles", (x) => RemoveFromTableById(x))
            };

            List<MenuElem> lekerdezesek = new List<MenuElem>()
            {
                new MenuElem("averageCostOfBrand", (x) => AverageCostOfBrand()),
                new MenuElem("numberOfUglyExtras", (x) => NumberOfUglyExtras()),
                new MenuElem("nameOfMostExpensiveModel", (x) => NameOfMostExpensiveModel())
            };

            gyoker.KovetkezoElemek = elsoSzint;
            elsoSzint[0].KovetkezoElemek = tablaFunkciok;
            tablaFunkciok[0].KovetkezoElemek = tablak;
            tablaFunkciok[1].KovetkezoElemek = lekerdezesek;
            tablak[0].KovetkezoElemek = muveletCollection;
            tablak[1].KovetkezoElemek = muveletCollection;
            tablak[2].KovetkezoElemek = muveletCollection;
            tablak[3].KovetkezoElemek = muveletCollection;

            // runs the menu
            while (MenuElem.JelenlegiElem.KovetkezoElemek != null)
            {
                int selectedMenu = 0;
                bool invalidData = true;

                while (invalidData)
                {
                    for (int i = 0; i < MenuElem.JelenlegiElem.KovetkezoElemek.Count(); i++)
                    {
                        Console.WriteLine(i + 1 + ". " + MenuElem.JelenlegiElem.KovetkezoElemek[i].Nev);
                    }

                    bool isNumber = int.TryParse(Console.ReadLine(), out selectedMenu);

                    if (isNumber && selectedMenu > 0 && selectedMenu <= MenuElem.JelenlegiElem.KovetkezoElemek.Count())
                    {
                        invalidData = false;
                    }

                    Console.Clear();
                }

                MenuElem.SelectMenu(selectedMenu - 1);
            }

            MenuElem.MenuActionParams.Clear();
        }

        /// <summary>
        /// This class can be used, to build menus, by instancing it, to create menu options, and using it static variables, to handle, and keep track of them
        /// </summary>
        public class MenuElem
        {
            /// <summary>
            /// Contains parameter. Can be blank
            /// </summary>
            private string menuParam;

            /// <summary>
            /// Initializes a new instance of the <see cref="MenuElem"/> class.
            ///  constructor, for menu options, that are only used for grouping, and has no other function
            /// </summary>
            /// <param name="nev">Name of the menu option.</param>
            public MenuElem(string nev)
            {
                this.Nev = nev;
            }

            /// <summary>
            /// Initializes a new instance of the <see cref="MenuElem"/> class.
            /// Used for initializing root menu option.
            /// </summary>
            /// <param name="nev">Name of the menu option.</param>
            /// <param name="gyokerElem">If true it is set to be the current menu option, else, it stays an empty menu option</param>
            public MenuElem(string nev, bool gyokerElem)
            {
                this.Nev = nev;
                if (gyokerElem)
                {
                    JelenlegiElem = this;
                }
            }

            /// <summary>
            /// Initializes a new instance of the <see cref="MenuElem"/> class.
            /// Constructor for menu options, that both have an action, and a parameter, that are used for this, or future actions
            /// </summary>
            /// <param name="nev">Name of the menu option</param>
            /// <param name="menuAction">The action, to be executed, upon selecting this menu options.</param>
            /// <param name="menuParam">Parameter of this instance</param>
            public MenuElem(string nev, Action<string[]> menuAction, string menuParam)
            {
                this.Nev = nev;
                this.MenuAction = menuAction;
                this.menuParam = menuParam;
            }

            /// <summary>
            /// Initializes a new instance of the <see cref="MenuElem"/> class.
            /// Constructor for menu options, that have an action executed upon selecting them
            /// </summary>
            /// <param name="nev">Name of the menu option</param>
            /// <param name="menuAction">The action, to be executed, upon selecting this menu options.</param>
            public MenuElem(string nev, Action<string[]> menuAction)
            {
                this.Nev = nev;
                this.MenuAction = menuAction;

                // this.menuParam = menuParam;
            }

            /// <summary>
            /// Initializes a new instance of the <see cref="MenuElem"/> class.
            /// Constructor for menu option, that doesnt have an action, but have parameters, that may be used by an action later on.
            /// </summary>
            /// <param name="nev">Name of the menu option.</param>
            /// <param name="menuParam">Parameter of this instance</param>
            public MenuElem(string nev, string menuParam)
            {
                this.Nev = nev;

                // this.MenuAction = MenuAction;
                this.menuParam = menuParam;
            }

            /// <summary>
            /// Gets or sets current menuOption
            /// </summary>
            public static MenuElem JelenlegiElem { get; set; }

            /// <summary>
            /// Gets or sets parameters for the menu
            /// </summary>
            public static List<string> MenuActionParams { get; set; } = new List<string>();

            /// <summary>
            /// Gets or sets the list that contains the new options, that will be shown, when selecting the current menu option
            /// </summary>
            public List<MenuElem> KovetkezoElemek { get; set; }

            /// <summary>
            /// Gets or sets menu option name
            /// </summary>
            public string Nev { get; set; }

            /// <summary>
            /// Gets Menu action, that is executed, upon selecting it. If null, nothing gets executed.
            /// </summary>
            public Action<string[]> MenuAction { get; }

            /// <summary>
            /// function that handles selecting an option
            /// </summary>
            /// <param name="index">Index of the selected parameter.</param>
            public static void SelectMenu(int index)
            {
                if (JelenlegiElem.menuParam != null)
                {
                    MenuActionParams.Add(JelenlegiElem.menuParam);
                }

                if (JelenlegiElem.KovetkezoElemek != null)
                {
                    JelenlegiElem = JelenlegiElem.KovetkezoElemek[index];
                }

                if (JelenlegiElem.KovetkezoElemek == null)
                {
                    JelenlegiElem.MenuAction?.Invoke(MenuActionParams.ToArray());
                }
            }
        }
    }
}
