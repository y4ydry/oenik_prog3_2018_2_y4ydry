var interface_car_shop_1_1_car_shop_logic_1_1_i_logic =
[
    [ "AverageCostOfBrand", "interface_car_shop_1_1_car_shop_logic_1_1_i_logic.html#a703cd98743fa439434356666a9dfb889", null ],
    [ "InsertBrand", "interface_car_shop_1_1_car_shop_logic_1_1_i_logic.html#a08515a02120c2afbf3c22d69c0566de3", null ],
    [ "InsertExtra", "interface_car_shop_1_1_car_shop_logic_1_1_i_logic.html#a19707eb23d5f66d076166cf94638bdb8", null ],
    [ "InsertExtraConnection", "interface_car_shop_1_1_car_shop_logic_1_1_i_logic.html#a162eada0dbccff6f443c7f98c5635349", null ],
    [ "InsertModel", "interface_car_shop_1_1_car_shop_logic_1_1_i_logic.html#ae1e28f1931405492593f610ff1d89a12", null ],
    [ "ModifyBrandById", "interface_car_shop_1_1_car_shop_logic_1_1_i_logic.html#a8bad556d5987f958de0c4f12150d8812", null ],
    [ "ModifyExtraById", "interface_car_shop_1_1_car_shop_logic_1_1_i_logic.html#a955c8931a49a5325c6c389dc9dab2bf5", null ],
    [ "ModifyModellById", "interface_car_shop_1_1_car_shop_logic_1_1_i_logic.html#ab5da41dc22a38f1b4e9ff8f3446c37ff", null ],
    [ "NameOfMostExpensiveModel", "interface_car_shop_1_1_car_shop_logic_1_1_i_logic.html#a324e907a468f633c3149b222a55b9e0c", null ],
    [ "NumberOfUglyExtras", "interface_car_shop_1_1_car_shop_logic_1_1_i_logic.html#a28cc06765ddc2f775887e4e6581670d2", null ],
    [ "ReadBrands", "interface_car_shop_1_1_car_shop_logic_1_1_i_logic.html#a44cc6a154b77010dae7660cebd34f626", null ],
    [ "ReadExtraConnections", "interface_car_shop_1_1_car_shop_logic_1_1_i_logic.html#a143dbaadec7834bc470b79cb391eded1", null ],
    [ "ReadExtras", "interface_car_shop_1_1_car_shop_logic_1_1_i_logic.html#aa854bf1cbdc2c38e2a6529c867cd6532", null ],
    [ "ReadModels", "interface_car_shop_1_1_car_shop_logic_1_1_i_logic.html#a7676484f39eda6221d271b633d05b56d", null ],
    [ "RemoveBrandById", "interface_car_shop_1_1_car_shop_logic_1_1_i_logic.html#aed2fba541dcfa1a6a06277090b2d2389", null ],
    [ "RemoveExtraById", "interface_car_shop_1_1_car_shop_logic_1_1_i_logic.html#a4ea69170a072c3b99c34f3f8866c910f", null ],
    [ "RemoveExtraConnectionById", "interface_car_shop_1_1_car_shop_logic_1_1_i_logic.html#a3a057b1ad229e9ad7847c292988fd13a", null ],
    [ "RemoveModellById", "interface_car_shop_1_1_car_shop_logic_1_1_i_logic.html#a9f2a16a0708a5ebeacbde64f3a7d606f", null ]
];