var class_car_shop_1_1_program_1_1_program =
[
    [ "MenuElem", "class_car_shop_1_1_program_1_1_program_1_1_menu_elem.html", "class_car_shop_1_1_program_1_1_program_1_1_menu_elem" ],
    [ "AverageCostOfBrand", "class_car_shop_1_1_program_1_1_program.html#a3bc540090771c5588e0d771a9a81b2fa", null ],
    [ "InsertIntoTable", "class_car_shop_1_1_program_1_1_program.html#a7abbfdbc2b425a2380437c5227cd6178", null ],
    [ "ListOffers", "class_car_shop_1_1_program_1_1_program.html#ac18fcc20ceb3df81fb05463e93adef44", null ],
    [ "ModifyTable", "class_car_shop_1_1_program_1_1_program.html#a7ca84bd346a2cbab4e29578a4d002981", null ],
    [ "NameOfMostExpensiveModel", "class_car_shop_1_1_program_1_1_program.html#a168c6181ddc13a7fc5b6822a6004b4c8", null ],
    [ "NumberOfUglyExtras", "class_car_shop_1_1_program_1_1_program.html#afd427f5f2c2ed551a0e732eeac8af61a", null ],
    [ "RemoveFromTableById", "class_car_shop_1_1_program_1_1_program.html#af311920a56296d7ddcaae33252d1e7e6", null ],
    [ "TableList", "class_car_shop_1_1_program_1_1_program.html#a0bff1ca5a6e2b547fb6aa61c7114481d", null ],
    [ "Logic", "class_car_shop_1_1_program_1_1_program.html#ac20d8b77fd7edab63be5bfc61502fd60", null ]
];