var interface_car_shop_1_1_repo_1_1_i_repository =
[
    [ "InsertBrand", "interface_car_shop_1_1_repo_1_1_i_repository.html#a462ec5b9725ffb1def2f7353245e9c21", null ],
    [ "InsertExtra", "interface_car_shop_1_1_repo_1_1_i_repository.html#a840f255450745aa22be8a6f932acc32a", null ],
    [ "InsertExtraConnection", "interface_car_shop_1_1_repo_1_1_i_repository.html#aa63f2c7b766d22a78ad79b939c3a729d", null ],
    [ "InsertModel", "interface_car_shop_1_1_repo_1_1_i_repository.html#af1ed0c12a851ca80a9ff6ad46fdae391", null ],
    [ "ModifyBrand< T >", "interface_car_shop_1_1_repo_1_1_i_repository.html#ae5c0d1f62e65e100a907b532252f7684", null ],
    [ "ModifyExtra< T >", "interface_car_shop_1_1_repo_1_1_i_repository.html#a28809aa9ff72028ef04fd2f654f2e37c", null ],
    [ "ModifyModel< T >", "interface_car_shop_1_1_repo_1_1_i_repository.html#a46f7497af8067a849d9a5ba2953f2cb0", null ],
    [ "ReadAllBrand", "interface_car_shop_1_1_repo_1_1_i_repository.html#ae25087f9fe94a9b06201c1282cc7204a", null ],
    [ "ReadAllExtra", "interface_car_shop_1_1_repo_1_1_i_repository.html#ad2fe20b2c43da2dfeff3761dbb5948bb", null ],
    [ "ReadAllExtraConnection", "interface_car_shop_1_1_repo_1_1_i_repository.html#a3e6d22e5474dcf6819614006b0b636ca", null ],
    [ "ReadAllModel", "interface_car_shop_1_1_repo_1_1_i_repository.html#aca83de9e175f6e92b941b9fdafc9bec4", null ],
    [ "RemoveBrand", "interface_car_shop_1_1_repo_1_1_i_repository.html#a3bdc62510b244db2074cfc49d098e9aa", null ],
    [ "RemoveExtra", "interface_car_shop_1_1_repo_1_1_i_repository.html#a52b0b4ee91964550010342f206d2ff68", null ],
    [ "RemoveExtraConnection", "interface_car_shop_1_1_repo_1_1_i_repository.html#ac77a96b9abb31ed09f85844820197af6", null ],
    [ "RemoveModel", "interface_car_shop_1_1_repo_1_1_i_repository.html#adb3553bad94ee0a6f28ae996cc26a334", null ]
];