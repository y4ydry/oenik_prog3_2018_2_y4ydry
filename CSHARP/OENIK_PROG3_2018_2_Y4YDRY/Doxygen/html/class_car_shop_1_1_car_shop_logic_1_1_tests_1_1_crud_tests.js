var class_car_shop_1_1_car_shop_logic_1_1_tests_1_1_crud_tests =
[
    [ "WhenInsertBrandIsCalled_TheRepoVersionIsCalledToo", "class_car_shop_1_1_car_shop_logic_1_1_tests_1_1_crud_tests.html#adc966a621424a41c6e9659d77452b4b4", null ],
    [ "WhenInsertExtraConnectionIsCalled_TheRepoVersionIsCalledToo", "class_car_shop_1_1_car_shop_logic_1_1_tests_1_1_crud_tests.html#aab8a98b98b54d451413617639b200b93", null ],
    [ "WhenInsertExtraIsCalled_TheRepoVersionIsCalledToo", "class_car_shop_1_1_car_shop_logic_1_1_tests_1_1_crud_tests.html#a446937b4e0de9f1ce4fc5849a769590a", null ],
    [ "WhenInsertModelIsCalled_TheRepoVersionIsCalledToo", "class_car_shop_1_1_car_shop_logic_1_1_tests_1_1_crud_tests.html#ad4f3a17e20307f49e7b65fd4da1eb1e6", null ],
    [ "WhenModifModelByIdIsCalled_ItsRepoMethodsAreCalledToo", "class_car_shop_1_1_car_shop_logic_1_1_tests_1_1_crud_tests.html#a628f5d75222d18c9906200b1f6bdca6c", null ],
    [ "WhenModifyBrandByIdIsCalled_ItsRepoMethodsAreCalledToo", "class_car_shop_1_1_car_shop_logic_1_1_tests_1_1_crud_tests.html#a2877a3d8935c2a5036462f2b7b521957", null ],
    [ "WhenModifyExtraByIdIsCalled_ItsRepoMethodsAreCalledToo", "class_car_shop_1_1_car_shop_logic_1_1_tests_1_1_crud_tests.html#aa9c44901e345d80fede75a17741d22bc", null ],
    [ "WhenReadBrandsIsCalled_TheRepoVersionIsCalledToo", "class_car_shop_1_1_car_shop_logic_1_1_tests_1_1_crud_tests.html#aeb427cb810a098955aa80f07ed5dd8b5", null ],
    [ "WhenReadExtraConnectionsIsCalled_TheRepoVersionIsCalledToo", "class_car_shop_1_1_car_shop_logic_1_1_tests_1_1_crud_tests.html#a1cdf252cc57e6b5ac5e40553a08e5ebf", null ],
    [ "WhenReadExtrasIsCalled_TheRepoVersionIsCalledToo", "class_car_shop_1_1_car_shop_logic_1_1_tests_1_1_crud_tests.html#a8deb4a6a5cb140b3e9cd38bed8a124b6", null ],
    [ "WhenReadModelsIsCalled_TheRepoVersionIsCalledToo", "class_car_shop_1_1_car_shop_logic_1_1_tests_1_1_crud_tests.html#a625d84da18800cdccb2462d07f995263", null ],
    [ "WhenRemoveBrandByIdIsCalledWithExistingId_RemoveBrandIsCalledToo", "class_car_shop_1_1_car_shop_logic_1_1_tests_1_1_crud_tests.html#a2d458b066df05ed3fafac9a4eba1cd82", null ],
    [ "WhenRemoveBrandByIdIsCalledWithInvalidId_ThrowsRecordDoesntExistException", "class_car_shop_1_1_car_shop_logic_1_1_tests_1_1_crud_tests.html#a209a7aa9eba3578ac6fb20ce28ac3123", null ],
    [ "WhenRemoveExtraByIdIsCalledWithExistingId_RemoveExtraIsCalledToo", "class_car_shop_1_1_car_shop_logic_1_1_tests_1_1_crud_tests.html#a517bd1e560c9beec6cce06a22e36e93a", null ],
    [ "WhenRemoveExtraByIdIsCalledWithInvalidId_ThrowsRecordDoesntExistException", "class_car_shop_1_1_car_shop_logic_1_1_tests_1_1_crud_tests.html#aa723b295ae3bce18b45458ccaac860c3", null ],
    [ "WhenRemoveExtraConnectionByIdIsCalledWithExistingId_RemoveExtraConnectionIsCalledToo", "class_car_shop_1_1_car_shop_logic_1_1_tests_1_1_crud_tests.html#a14a491466d6785c622b407046e513202", null ],
    [ "WhenRemoveExtraConnectionByIdIsCalledWithInvalidId_ThrowsRecordDoesntExistException", "class_car_shop_1_1_car_shop_logic_1_1_tests_1_1_crud_tests.html#a78342ab320159a43f54207a3f01c330e", null ],
    [ "WhenRemoveModelByIdIsCalledWithExistingId_RemoveModelIsCalledToo", "class_car_shop_1_1_car_shop_logic_1_1_tests_1_1_crud_tests.html#adc1d1e24fca58513ea1bbbb1df538431", null ],
    [ "WhenRemoveModelByIdIsCalledWithInvalidId_ThrowsRecordDoesntExistException", "class_car_shop_1_1_car_shop_logic_1_1_tests_1_1_crud_tests.html#ac4081960f1930726233e9cd5d0722e35", null ]
];