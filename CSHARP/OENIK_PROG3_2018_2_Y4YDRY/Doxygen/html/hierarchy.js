var hierarchy =
[
    [ "CarShop.Data.AUTOMARKAK", "class_car_shop_1_1_data_1_1_a_u_t_o_m_a_r_k_a_k.html", null ],
    [ "CarShop.CarShopLogic.Tests.CrudTests", "class_car_shop_1_1_car_shop_logic_1_1_tests_1_1_crud_tests.html", null ],
    [ "DbContext", null, [
      [ "CarShop.Data.CarShopDataEntities", "class_car_shop_1_1_data_1_1_car_shop_data_entities.html", null ]
    ] ],
    [ "Exception", null, [
      [ "CarShop.CarShopLogic.RecordDoesntExistException", "class_car_shop_1_1_car_shop_logic_1_1_record_doesnt_exist_exception.html", null ]
    ] ],
    [ "CarShop.Data.EXTRAK", "class_car_shop_1_1_data_1_1_e_x_t_r_a_k.html", null ],
    [ "IDisposable", null, [
      [ "CarShop.Repo.Repository", "class_car_shop_1_1_repo_1_1_repository.html", null ]
    ] ],
    [ "CarShop.CarShopLogic.ILogic", "interface_car_shop_1_1_car_shop_logic_1_1_i_logic.html", [
      [ "CarShop.CarShopLogic.Logic", "class_car_shop_1_1_car_shop_logic_1_1_logic.html", null ]
    ] ],
    [ "CarShop.Repo.IRepository", "interface_car_shop_1_1_repo_1_1_i_repository.html", [
      [ "CarShop.Repo.Repository", "class_car_shop_1_1_repo_1_1_repository.html", null ]
    ] ],
    [ "CarShop.Program.Program.MenuElem", "class_car_shop_1_1_program_1_1_program_1_1_menu_elem.html", null ],
    [ "CarShop.Data.MODELL_EXTRA_KAPCSOLO", "class_car_shop_1_1_data_1_1_m_o_d_e_l_l___e_x_t_r_a___k_a_p_c_s_o_l_o.html", null ],
    [ "CarShop.Data.MODELLEK", "class_car_shop_1_1_data_1_1_m_o_d_e_l_l_e_k.html", null ],
    [ "CarShop.Program.Program", "class_car_shop_1_1_program_1_1_program.html", null ],
    [ "CarShop.CarShopLogic.Tests.QuerryTests", "class_car_shop_1_1_car_shop_logic_1_1_tests_1_1_querry_tests.html", null ]
];