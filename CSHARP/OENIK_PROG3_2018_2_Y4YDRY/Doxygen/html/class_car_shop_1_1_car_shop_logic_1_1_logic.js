var class_car_shop_1_1_car_shop_logic_1_1_logic =
[
    [ "Logic", "class_car_shop_1_1_car_shop_logic_1_1_logic.html#a83a53bc007a78e4d20cd99828085d98c", null ],
    [ "AverageCostOfBrand", "class_car_shop_1_1_car_shop_logic_1_1_logic.html#a442ec187ce19e687c1b3c60ee6686030", null ],
    [ "InsertBrand", "class_car_shop_1_1_car_shop_logic_1_1_logic.html#ab1aa4df67e7c1f7a19a273a5cc8f985a", null ],
    [ "InsertExtra", "class_car_shop_1_1_car_shop_logic_1_1_logic.html#a4e167d42b5520221a9f5d45a6ef4b871", null ],
    [ "InsertExtraConnection", "class_car_shop_1_1_car_shop_logic_1_1_logic.html#a1bd69693cc5e2092c8f9fd52a560721d", null ],
    [ "InsertModel", "class_car_shop_1_1_car_shop_logic_1_1_logic.html#af4b1d14977c7bdfe43f77bd72daf8868", null ],
    [ "ModifyBrandById", "class_car_shop_1_1_car_shop_logic_1_1_logic.html#aab5028f91f5d45d8587c6e3d025c7e47", null ],
    [ "ModifyExtraById", "class_car_shop_1_1_car_shop_logic_1_1_logic.html#a5ab078afea74d05ba46169c156a7791e", null ],
    [ "ModifyModellById", "class_car_shop_1_1_car_shop_logic_1_1_logic.html#ac2b3a8e47f215bdcf045ac8808940ec4", null ],
    [ "NameOfMostExpensiveModel", "class_car_shop_1_1_car_shop_logic_1_1_logic.html#a9228eca7474626ec622a69ae02081e2e", null ],
    [ "NumberOfUglyExtras", "class_car_shop_1_1_car_shop_logic_1_1_logic.html#a9a447775a24e1dba2a7dc3b6244fec97", null ],
    [ "ReadBrands", "class_car_shop_1_1_car_shop_logic_1_1_logic.html#ad04450f04414fc3a87df550c4e875b20", null ],
    [ "ReadExtraConnections", "class_car_shop_1_1_car_shop_logic_1_1_logic.html#a7df21d0b9a2f54e94eb146adb0b60d03", null ],
    [ "ReadExtras", "class_car_shop_1_1_car_shop_logic_1_1_logic.html#a6aa0297e40449fc389af4ec78485bad9", null ],
    [ "ReadModels", "class_car_shop_1_1_car_shop_logic_1_1_logic.html#ae73d879a6a4bda1333dd6d16219a2501", null ],
    [ "RemoveBrandById", "class_car_shop_1_1_car_shop_logic_1_1_logic.html#ab6da147820a16bbb675f6d34a5613dcf", null ],
    [ "RemoveExtraById", "class_car_shop_1_1_car_shop_logic_1_1_logic.html#a6f15bce0eb84174bc716efc63bb720b0", null ],
    [ "RemoveExtraConnectionById", "class_car_shop_1_1_car_shop_logic_1_1_logic.html#ac9825903341098817a5a0417dd37c3ba", null ],
    [ "RemoveModellById", "class_car_shop_1_1_car_shop_logic_1_1_logic.html#a6c43a579ca786a03939b67841bc78888", null ]
];