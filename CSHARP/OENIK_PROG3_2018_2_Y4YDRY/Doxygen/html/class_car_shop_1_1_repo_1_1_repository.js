var class_car_shop_1_1_repo_1_1_repository =
[
    [ "Repository", "class_car_shop_1_1_repo_1_1_repository.html#a0bcb0597964f78520063ef0f92668c78", null ],
    [ "Repository", "class_car_shop_1_1_repo_1_1_repository.html#a5e2238c83873858cf8289bcf37338985", null ],
    [ "Dispose", "class_car_shop_1_1_repo_1_1_repository.html#a1d44b596acfecb3355cf985b695d241c", null ],
    [ "InsertBrand", "class_car_shop_1_1_repo_1_1_repository.html#a31aed1de8d96c75cbd2293b2b54dc4b7", null ],
    [ "InsertExtra", "class_car_shop_1_1_repo_1_1_repository.html#a2e153b24ef8c2ce1249eeeab058a4306", null ],
    [ "InsertExtraConnection", "class_car_shop_1_1_repo_1_1_repository.html#aa2e9d4873a0a17b2592f8a0c3071f643", null ],
    [ "InsertModel", "class_car_shop_1_1_repo_1_1_repository.html#a7613e8fc8bcc880bfb52d86b32d52b44", null ],
    [ "ModifyBrand< T >", "class_car_shop_1_1_repo_1_1_repository.html#a221efb02818e0b6e22651013ae924744", null ],
    [ "ModifyExtra< T >", "class_car_shop_1_1_repo_1_1_repository.html#a197e3dbc3006929b2ae2bd076bb4f7af", null ],
    [ "ModifyModel< T >", "class_car_shop_1_1_repo_1_1_repository.html#a8f60b42b756b4590d9b935493fac6ea8", null ],
    [ "ReadAllBrand", "class_car_shop_1_1_repo_1_1_repository.html#a1a01fee56492e68c293a0996f6f81bc4", null ],
    [ "ReadAllExtra", "class_car_shop_1_1_repo_1_1_repository.html#a0ac56bbd1b8d8efd5e32eb17f29c1bf4", null ],
    [ "ReadAllExtraConnection", "class_car_shop_1_1_repo_1_1_repository.html#ac73bb8c10eca9614c97bb405201eaadd", null ],
    [ "ReadAllModel", "class_car_shop_1_1_repo_1_1_repository.html#ab67fca02dbb7ccc9d5cb14ea4b950cb0", null ],
    [ "RemoveBrand", "class_car_shop_1_1_repo_1_1_repository.html#a61c82335912dad730a14028374d75fd2", null ],
    [ "RemoveExtra", "class_car_shop_1_1_repo_1_1_repository.html#ad8bd122620601d7ab9e1f258a22c5fac", null ],
    [ "RemoveExtraConnection", "class_car_shop_1_1_repo_1_1_repository.html#a40c754343b2368923edcf7369e04126a", null ],
    [ "RemoveModel", "class_car_shop_1_1_repo_1_1_repository.html#a3df33fa2098366bd9194012458b4e904", null ]
];