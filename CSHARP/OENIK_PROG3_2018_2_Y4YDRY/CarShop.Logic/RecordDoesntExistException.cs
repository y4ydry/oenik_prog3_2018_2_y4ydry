﻿// <copyright file="RecordDoesntExistException.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace CarShop.CarShopLogic
{
    using System;
    using System.Collections.Generic;
    using System.Data.Entity;
    using System.Linq;
    using System.Reflection;
    using System.Text;
    using System.Threading.Tasks;
    using CarShop.Data;
    using CarShop.Repo;

    /// <summary>
    /// thrown when trying to acces a record, that doesnt exists
    /// </summary>
    [Serializable]
    public class RecordDoesntExistException : Exception
    {
    }
}
