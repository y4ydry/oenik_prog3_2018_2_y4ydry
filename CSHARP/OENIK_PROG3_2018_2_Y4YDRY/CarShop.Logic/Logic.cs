﻿// <copyright file="Logic.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace CarShop.CarShopLogic
{
    using System;
    using System.Collections.Generic;
    using System.Data.Entity;
    using System.Linq;
    using System.Reflection;
    using System.Text;
    using System.Threading.Tasks;
    using CarShop.Data;
    using CarShop.Repo;

    /// <summary>
    /// Logic class for CarShop
    /// </summary>
    public class Logic : ILogic
    {
        private readonly IRepository repo;

        /// <summary>
        /// Initializes a new instance of the <see cref="Logic"/> class.
        /// Constructor, that sets the repository, to the given value
        /// </summary>
        /// <param name="repo">repository to use</param>
        public Logic(IRepository repo)
        {
            this.repo = repo;
        }

        /// <summary>
        /// Insert brand, by giving all its parameters
        /// </summary>
        /// <param name="id">id</param>
        /// <param name="nev">name</param>
        /// <param name="alapitasEve">year of creation</param>
        /// <param name="url">url</param>
        /// <param name="evesForgalom">yearly traffic</param>
        /// <param name="orszagnev">country name</param>
        public void InsertBrand(decimal id, string nev, decimal alapitasEve, string url, decimal evesForgalom, string orszagnev)
        {
            this.repo.InsertBrand(
                            new AUTOMARKAK()
                            {
                                NEV = nev,
                                ALAPITAS_EVE = alapitasEve,
                                C_URL = url,
                                EVES_FORGALOM = evesForgalom,
                                ID = id,
                                ORSZAGNEV = orszagnev
                            });
        }

        /// <summary>
        /// Insert extra, by giving all its parameters.
        /// </summary>
        /// <param name="id">id</param>
        /// <param name="kategoriaNev">category name</param>
        /// <param name="nev">name</param>
        /// <param name="ar">cost</param>
        /// <param name="szin">color</param>
        /// <param name="igenytelenE">Is it ugly</param>
        public void InsertExtra(decimal id, string kategoriaNev, string nev, decimal ar, string szin, bool igenytelenE)
        {
            this.repo.InsertExtra(
                new EXTRAK()
                {
                    ID = id,
                    KATEGORIA_NEV = kategoriaNev,
                    NEV = nev,
                    AR = ar,
                    SZIN = szin,
                    IGENYTELEN_E = igenytelenE
                });
        }

        /// <summary>
        /// insert model extra connection, by giving all its parameters
        /// </summary>
        /// <param name="id">od</param>
        /// <param name="modelId">modelId</param>
        /// <param name="extraId">extraId</param>
        public void InsertExtraConnection(decimal id, decimal modelId, decimal extraId)
        {
            this.repo.InsertExtraConnection(
                new MODELL_EXTRA_KAPCSOLO()
                {
                    ID = id,
                    MODELL_ID = modelId,
                    EXTRA_ID = extraId
                });
        }

        /// <summary>
        /// Insert model, by giving all its parameters
        /// </summary>
        /// <param name="id">id</param>
        /// <param name="markaId">brand id</param>
        /// <param name="nev">name</param>
        /// <param name="megjelenesEve">year of publication</param>
        /// <param name="motorTerfogat">engine power</param>
        /// <param name="loero">horsepower</param>
        /// <param name="alapar">base cost</param>
        public void InsertModel(decimal id, decimal markaId, string nev, decimal megjelenesEve, decimal motorTerfogat, decimal loero, decimal alapar)
        {
            this.repo.InsertModel(new MODELLEK
            {
                ID = id,
                MARKA_ID = markaId,
                NEV = nev,
                MEGJELENES_EVE = megjelenesEve,
                MOTOR_TERFOGAT = motorTerfogat,
                LOERO = loero,
                ALAPAR = alapar
            });
        }

        /// <summary>
        /// get brands
        /// </summary>
        /// <returns>a quearyable list</returns>
        public IQueryable<AUTOMARKAK> ReadBrands()
        {
            return this.repo.ReadAllBrand();
        }

        /// <summary>
        /// get model extra connections
        /// </summary>
        /// <returns>a quearyable list</returns>
        public IQueryable<MODELL_EXTRA_KAPCSOLO> ReadExtraConnections()
        {
            return this.repo.ReadAllExtraConnection();
        }

        /// <summary>
        /// get extras
        /// </summary>
        /// <returns>a quearyable list</returns>
        public IQueryable<EXTRAK> ReadExtras()
        {
           return this.repo.ReadAllExtra();
        }

        /// <summary>
        /// get models
        /// </summary>
        /// <returns>a quearyable list</returns>
        public IQueryable<MODELLEK> ReadModels()
        {
            return this.repo.ReadAllModel();
        }

        /// <summary>
        /// Function for modifying brands by id.
        /// </summary>
        /// <param name="id">id</param>
        /// <param name="columnName">Column name to be modified</param>
        /// <param name="changeTo">New value</param>
        public void ModifyBrandById(int id, string columnName, string changeTo)
        {
            this.repo.ModifyBrand(id, columnName, changeTo);
        }

        /// <summary>
        /// Function for modifying extras by id.
        /// </summary>
        /// <param name="id">id</param>
        /// <param name="columnName">Column name to be modified</param>
        /// <param name="changeTo">New value</param>
        public void ModifyExtraById(int id, string columnName, string changeTo)
        {
            this.repo.ModifyExtra(id, columnName, changeTo);
        }

        /// <summary>
        /// Function for modifying models by id.
        /// </summary>
        /// <param name="id">id</param>
        /// <param name="columnName">Column name to be modified</param>
        /// <param name="changeTo">New value</param>
        public void ModifyModellById(int id, string columnName, string changeTo)
        {
            this.repo.ModifyModel(id, columnName, changeTo);
        }

        /// <summary>
        /// Remove brand by id. If it doesnt exists throw an exception.
        /// </summary>
        /// <param name="id">id</param>
        public void RemoveBrandById(decimal id)
        {
            var q = this.repo.ReadAllBrand().Where(x => x.ID == id);
            if (q.Count() == 0)
            {
                throw new RecordDoesntExistException();
            }
            else
            {
                this.repo.RemoveBrand(q.First());
            }
        }

        /// <summary>
        /// Remove extra by id. If it doesnt exists throw an exception.
        /// </summary>
        /// <param name="id">id</param>
        public void RemoveExtraById(decimal id)
        {
            var q = this.repo.ReadAllExtra().Where(x => x.ID == id);
            if (q.Count() == 0)
            {
                throw new RecordDoesntExistException();
            }
            else
            {
                this.repo.RemoveExtra(q.First());
            }
        }

        /// <summary>
        /// Remove model extra connection by id. If it doesnt exists throw an exception.
        /// </summary>
        /// <param name="id">id</param>
        public void RemoveExtraConnectionById(decimal id)
        {
            var q = this.repo.ReadAllExtraConnection().Where(x => x.ID == id);
            if (q.Count() == 0)
            {
                throw new RecordDoesntExistException();
            }
            else
            {
                this.repo.RemoveExtraConnection(q.First());
            }
        }

        /// <summary>
        /// Remove model by id. If it doesnt exists throw an exception.
        /// </summary>
        /// <param name="id">id</param>
        public void RemoveModellById(decimal id)
        {
            var q = this.repo.ReadAllModel().Where(x => x.ID == id);
            if (q.Count() == 0)
            {
                throw new RecordDoesntExistException();
            }
            else
            {
                this.repo.RemoveModel(q.First());
            }
        }

        /// <summary>
        /// returns the average cost of a brand that was given
        /// </summary>
        /// <param name="id">Id of record</param>
        /// <returns> returns the average cost of the brand given</returns>
        public decimal AverageCostOfBrand(int id)
        {
            var q = this.ReadModels().Where(x => x.MARKA_ID == id);

            return (decimal)q.Average(x => x.ALAPAR);
        }

        /// <summary>
        /// returns the number of ugly extras
        /// </summary>
        /// <returns>number of ugly extras</returns>
        public int NumberOfUglyExtras()
        {
            return this.ReadExtras().Where(x => x.IGENYTELEN_E == true).Count();
        }

        /// <summary>
        /// returns the name of the most expensive model
        /// </summary>
        /// <returns>name of the most expensive model</returns>
        public string NameOfMostExpensiveModel()
        {
            var table = this.ReadModels();
            var max = table.Max(x => x.ALAPAR).Value;
            var q = table.Where(x => x.ALAPAR == max).First();
            return q.NEV;
        }
    }
}
