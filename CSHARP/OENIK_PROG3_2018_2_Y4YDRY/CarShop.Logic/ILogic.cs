﻿// <copyright file="ILogic.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace CarShop.CarShopLogic
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using CarShop.Data;

    /// <summary>
    /// functions for CarShop logic
    /// </summary>
    public interface ILogic
    {
        /// <summary>
        /// Insert brand, by giving all its parameters
        /// </summary>
        /// <param name="id">id</param>
        /// <param name="nev">name</param>
        /// <param name="alapitasEve">year of creation</param>
        /// <param name="url">url</param>
        /// <param name="evesForgalom">yearly traffic</param>
        /// <param name="orszagnev">country name</param>
        void InsertBrand(decimal id, string nev, decimal alapitasEve, string url, decimal evesForgalom, string orszagnev);

        /// <summary>
        /// Insert model, by giving all its parameters
        /// </summary>
        /// <param name="id">id</param>
        /// <param name="markaId">brand id</param>
        /// <param name="nev">name</param>
        /// <param name="megjelenesEve">year of publication</param>
        /// <param name="motorTerfogat">engine power</param>
        /// <param name="loero">horsepower</param>
        /// <param name="alapar">base cost</param>
        void InsertModel(decimal id, decimal markaId, string nev, decimal megjelenesEve, decimal motorTerfogat, decimal loero, decimal alapar);

        /// <summary>
        /// Insert extra, by giving all its parameters.
        /// </summary>
        /// <param name="id">id</param>
        /// <param name="kategoriaNev">category name</param>
        /// <param name="nev">name</param>
        /// <param name="ar">cost</param>
        /// <param name="szin">color</param>
        /// <param name="igenytelenE">Is it ugly</param>
        void InsertExtra(decimal id, string kategoriaNev, string nev, decimal ar, string szin, bool igenytelenE);

        /// <summary>
        /// insert model extra connection, by giving all its parameters
        /// </summary>
        /// <param name="id">od</param>
        /// <param name="modelId">modelId</param>
        /// <param name="extraId">extraId</param>
        void InsertExtraConnection(decimal id, decimal modelId, decimal extraId);

        /// <summary>
        /// remove brand by id
        /// </summary>
        /// <param name="id">id</param>
        void RemoveBrandById(decimal id);

        /// <summary>
        /// remove model by id
        /// </summary>
        /// <param name="id">id</param>
        void RemoveModellById(decimal id);

        /// <summary>
        /// remove extra by id
        /// </summary>
        /// <param name="id">id</param>
        void RemoveExtraById(decimal id);

        /// <summary>
        /// remove model extra connection by id
        /// </summary>
        /// <param name="id">id</param>
        void RemoveExtraConnectionById(decimal id);

        /// <summary>
        /// Function for modifying brands.
        /// </summary>
        /// <param name="id">id</param>
        /// <param name="columnName">Column name to be modified</param>
        /// <param name="changeTo">New value</param>
        void ModifyBrandById(int id, string columnName, string changeTo);

        /// <summary>
        /// Function for modifying models.
        /// </summary>
        /// <param name="id">id</param>
        /// <param name="columnName">Column name to be modified</param>
        /// <param name="changeTo">New value</param>
        void ModifyModellById(int id, string columnName, string changeTo);

        /// <summary>
        /// Function for modifying extras.
        /// </summary>
        /// <param name="id">id</param>
        /// <param name="columnName">Column name to be modified</param>
        /// <param name="changeTo">New value</param>
        void ModifyExtraById(int id, string columnName, string changeTo);

        /// <summary>
        /// Function for listing  brands.
        /// </summary>
        /// <returns> a quearyable list</returns>
        IQueryable<AUTOMARKAK> ReadBrands();

        /// <summary>
        /// Function for listing  models.
        /// </summary>
        /// <returns> a quearyable list</returns>
        IQueryable<MODELLEK> ReadModels();

        /// <summary>
        /// Function for listing  extras.
        /// </summary>
        /// <returns> a quearyable list</returns>
        IQueryable<EXTRAK> ReadExtras();

        /// <summary>
        /// Function for listing  model extra connection.
        /// </summary>
        /// <returns> a quearyable list</returns>
        IQueryable<MODELL_EXTRA_KAPCSOLO> ReadExtraConnections();

        /// <summary>
        /// returns the average cost of a brand that was given
        /// </summary>
        /// <param name="id">Id of record</param>
        /// <returns> returns the average cost of the brand given</returns>
        decimal AverageCostOfBrand(int id);

        /// <summary>
        /// returns the number of ugly extras
        /// </summary>
        /// <returns>number of ugly extras</returns>
        int NumberOfUglyExtras();

        /// <summary>
        /// returns the name of the most expensive model
        /// </summary>
        /// <returns>name of the most expensive model</returns>
        string NameOfMostExpensiveModel();
    }
}
