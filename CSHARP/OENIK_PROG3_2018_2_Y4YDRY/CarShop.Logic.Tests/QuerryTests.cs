﻿// <copyright file="QuerryTests.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace CarShop.CarShopLogic.Tests
{
    using System.Collections.Generic;
    using System.Linq;
    using CarShop.CarShopLogic;
    using CarShop.Data;
    using CarShop.Repo;
    using Moq;
    using NUnit.Framework;

    /// <summary>
    /// tests for the querries
    /// </summary>
    [TestFixture]
    internal class QuerryTests
    {
            /// <summary>
            /// tests if AverageCostOfBrandTest returns the expected value, and if it invokes the required repo methonds
            /// </summary>
        [Test]
        public void AverageCostOfBrandTest()
        {
            Mock<IRepository> repoMock = new Mock<IRepository>();

            List<MODELLEK> table = new List<MODELLEK>()
            {
                new MODELLEK { ID = 1, NEV = "tesztnev", ALAPAR = 100, LOERO = 123, MARKA_ID = 1, MEGJELENES_EVE = 2000, MOTOR_TERFOGAT = 123 },
                new MODELLEK { ID = 2, NEV = "tesztne2v", ALAPAR = 200, LOERO = 123, MARKA_ID = 1, MEGJELENES_EVE = 2000, MOTOR_TERFOGAT = 123 }
            };

            repoMock.Setup(x => x.ReadAllModel()).Returns(table.AsQueryable());
            var logic = new Logic(repoMock.Object);

            decimal eredmeny = logic.AverageCostOfBrand(1);

            Assert.That(eredmeny, Is.EqualTo(150));
            Assert.That(eredmeny, Is.Not.EqualTo(151));
            repoMock.Verify(x => x.ReadAllModel(), Times.Once());
        }

        /// <summary>
        /// tests if NumberOfUglyExtrasTest returns the expected value, and if it invokes the required repo methonds
        /// </summary>
        [Test]
        public void NumberOfUglyExtrasTest()
        {
            Mock<IRepository> repoMock = new Mock<IRepository>();

            List<EXTRAK> table = new List<EXTRAK>()
            {
                new EXTRAK() { ID = 1, NEV = "tesztnev", KATEGORIA_NEV = "tesztkategoria", AR = 123, IGENYTELEN_E = true, SZIN = "tesztszin" },
                new EXTRAK() { ID = 2, NEV = "tesztnev", KATEGORIA_NEV = "tesztkategoria", AR = 123, IGENYTELEN_E = true, SZIN = "tesztszin" },
                new EXTRAK() { ID = 3, NEV = "tesztnev", KATEGORIA_NEV = "tesztkategoria", AR = 123, IGENYTELEN_E = false, SZIN = "tesztszin" },
                new EXTRAK() { ID = 4, NEV = "tesztnev", KATEGORIA_NEV = "tesztkategoria", AR = 123, IGENYTELEN_E = true, SZIN = "tesztszin" }
            };

            repoMock.Setup(x => x.ReadAllExtra()).Returns(table.AsQueryable());
            var logic = new Logic(repoMock.Object);

            decimal eredmeny = logic.NumberOfUglyExtras();

            Assert.That(eredmeny, Is.EqualTo(3));
            Assert.That(eredmeny, Is.Not.EqualTo(4));
            repoMock.Verify(x => x.ReadAllExtra(), Times.Once());
        }

        /// <summary>
        /// tests if NameOfMostExpensiveModelTest returns the expected value, and if it invokes the required repo methonds
        /// </summary>
        [Test]
        public void NameOfMostExpensiveModelTest()
        {
            Mock<IRepository> repoMock = new Mock<IRepository>();

            List<MODELLEK> table = new List<MODELLEK>()
            {
                new MODELLEK { ID = 1, NEV = "tesztnev", ALAPAR = 100, LOERO = 123, MARKA_ID = 1, MEGJELENES_EVE = 2000, MOTOR_TERFOGAT = 123 },
                new MODELLEK { ID = 2, NEV = "tesztnev2", ALAPAR = 1000, LOERO = 123, MARKA_ID = 1, MEGJELENES_EVE = 2000, MOTOR_TERFOGAT = 123 },
                new MODELLEK { ID = 2, NEV = "tesztnev3", ALAPAR = 200, LOERO = 123, MARKA_ID = 1, MEGJELENES_EVE = 2000, MOTOR_TERFOGAT = 123 }
            };

            repoMock.Setup(x => x.ReadAllModel()).Returns(table.AsQueryable());
            var logic = new Logic(repoMock.Object);

            string eredmeny = logic.NameOfMostExpensiveModel();

            Assert.That(eredmeny, Is.EqualTo("tesztnev2"));
            Assert.That(eredmeny, Is.Not.EqualTo("tesztnev1"));
            repoMock.Verify(x => x.ReadAllModel(), Times.Once());
        }
    }
}
