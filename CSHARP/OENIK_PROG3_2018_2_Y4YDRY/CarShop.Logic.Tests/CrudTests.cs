﻿// <copyright file="CrudTests.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace CarShop.CarShopLogic.Tests
{
    using System.Collections.Generic;
    using System.Linq;
    using CarShop.CarShopLogic;
    using CarShop.Data;
    using CarShop.Repo;
    using Moq;
    using NUnit.Framework;

    /// <summary>
    /// Tests for Crud logic functions
    /// </summary>
    [TestFixture]
    internal class CrudTests
    {
        /// <summary>
        /// Checks if the repository functions is called
        /// </summary>
        [Test]
        public void WhenInsertBrandIsCalled_TheRepoVersionIsCalledToo()
        {
            Mock<IRepository> repoMock = new Mock<IRepository>();

            var logic = new Logic(repoMock.Object);
            logic.InsertBrand(3, "tesztnev3", 1998, "teszt.url3", 53, "tesztorszag3");

            repoMock.Verify(x => x.InsertBrand(It.IsAny<AUTOMARKAK>()), Times.Once);
        }

        /// <summary>
        /// Checks if the repository functions is called
        /// </summary>
        [Test]
        public void WhenInsertExtraIsCalled_TheRepoVersionIsCalledToo()
        {
            Mock<IRepository> repoMock = new Mock<IRepository>();

            var logic = new Logic(repoMock.Object);
            logic.InsertExtra(1, "tesztKategoria", "Tesztnev", 12, "piros", true);

            repoMock.Verify(x => x.InsertExtra(It.IsAny<EXTRAK>()), Times.Once);
        }

        /// <summary>
        /// Checks if the repository functions is called
        /// </summary>
        [Test]
        public void WhenInsertExtraConnectionIsCalled_TheRepoVersionIsCalledToo()
        {
            Mock<IRepository> repoMock = new Mock<IRepository>();

            var logic = new Logic(repoMock.Object);
            logic.InsertExtraConnection(1, 2, 3);

            repoMock.Verify(x => x.InsertExtraConnection(It.IsAny<MODELL_EXTRA_KAPCSOLO>()), Times.Once);
        }

        /// <summary>
        /// Checks if the repository functions is called
        /// </summary>
        [Test]
        public void WhenInsertModelIsCalled_TheRepoVersionIsCalledToo()
        {
            Mock<IRepository> repoMock = new Mock<IRepository>();

            var logic = new Logic(repoMock.Object);
            logic.InsertModel(1, 2, "teszt", 1998, 100, 150, 2000000);

            repoMock.Verify(x => x.InsertModel(It.IsAny<MODELLEK>()), Times.Once);
        }

        /// <summary>
        /// Checks if the repository functions is called
        /// </summary>
        [Test]
        public void WhenReadBrandsIsCalled_TheRepoVersionIsCalledToo()
        {
            Mock<IRepository> repoMock = new Mock<IRepository>();

            var logic = new Logic(repoMock.Object);
            logic.ReadBrands();

            repoMock.Verify(x => x.ReadAllBrand(), Times.Once);
        }

        /// <summary>
        /// Checks if the repository functions is called
        /// </summary>
        [Test]
        public void WhenReadExtraConnectionsIsCalled_TheRepoVersionIsCalledToo()
        {
            Mock<IRepository> repoMock = new Mock<IRepository>();

            var logic = new Logic(repoMock.Object);
            logic.ReadExtraConnections();

            repoMock.Verify(x => x.ReadAllExtraConnection(), Times.Once);
        }

        /// <summary>
        /// Checks if the repository functions is called
        /// </summary>
        [Test]
        public void WhenReadExtrasIsCalled_TheRepoVersionIsCalledToo()
        {
            Mock<IRepository> repoMock = new Mock<IRepository>();

            var logic = new Logic(repoMock.Object);
            logic.ReadExtras();

            repoMock.Verify(x => x.ReadAllExtra(), Times.Once);
        }

        /// <summary>
        /// Checks if the repository functions is called
        /// </summary>
        [Test]
        public void WhenReadModelsIsCalled_TheRepoVersionIsCalledToo()
        {
            Mock<IRepository> repoMock = new Mock<IRepository>();

            var logic = new Logic(repoMock.Object);
            logic.ReadModels();

            repoMock.Verify(x => x.ReadAllModel(), Times.Once);
        }

        /// <summary>
        /// Checks if the repository functions is called
        /// </summary>
        [Test]
        public void WhenRemoveBrandByIdIsCalledWithExistingId_RemoveBrandIsCalledToo()
        {
            Mock<IRepository> repoMock = new Mock<IRepository>();

            List<AUTOMARKAK> table = new List<AUTOMARKAK>()
            {
                new AUTOMARKAK() { ID = 1, NEV = "tesztnev", ALAPITAS_EVE = 1997, EVES_FORGALOM = 1, C_URL = "teszturl", ORSZAGNEV = "tesztország" },
                new AUTOMARKAK() { ID = 2, NEV = "tesztnev2", ALAPITAS_EVE = 19927, EVES_FORGALOM = 12, C_URL = "teszturl2", ORSZAGNEV = "tesztország2" }
            };

            repoMock.Setup(x => x.ReadAllBrand()).Returns(table.AsQueryable());

            var logic = new Logic(repoMock.Object);

            logic.RemoveBrandById(2);

            repoMock.Verify(x => x.RemoveBrand(It.IsAny<AUTOMARKAK>()), Times.Once());
            repoMock.Verify(x => x.ReadAllBrand(), Times.Once());
        }

        /// <summary>
        /// Checks if the repository functions is called
        /// </summary>
        [Test]
        public void WhenRemoveBrandByIdIsCalledWithInvalidId_ThrowsRecordDoesntExistException()
        {
                Mock<IRepository> repoMock = new Mock<IRepository>();

                List<AUTOMARKAK> table = new List<AUTOMARKAK>()
            {
                new AUTOMARKAK() { ID = 1, NEV = "tesztnev", ALAPITAS_EVE = 1997, EVES_FORGALOM = 1, C_URL = "teszturl", ORSZAGNEV = "tesztország" },
                new AUTOMARKAK() { ID = 2, NEV = "tesztnev2", ALAPITAS_EVE = 19927, EVES_FORGALOM = 12, C_URL = "teszturl2", ORSZAGNEV = "tesztország2" }
            };

                repoMock.Setup(x => x.ReadAllBrand()).Returns(table.AsQueryable());

                var logic = new Logic(repoMock.Object);

                Assert.Throws<RecordDoesntExistException>(() => logic.RemoveBrandById(3));
        }

        /// <summary>
        /// Checks if the repository functions is called
        /// </summary>
        [Test]
        public void WhenRemoveModelByIdIsCalledWithExistingId_RemoveModelIsCalledToo()
        {
            Mock<IRepository> repoMock = new Mock<IRepository>();

            List<MODELLEK> table = new List<MODELLEK>()
            {
                new MODELLEK { ID = 1, NEV = "tesztnev", ALAPAR = 123, LOERO = 123, MARKA_ID = 1, MEGJELENES_EVE = 2000, MOTOR_TERFOGAT = 123 },
                new MODELLEK { ID = 2, NEV = "tesztne2v", ALAPAR = 123, LOERO = 123, MARKA_ID = 1, MEGJELENES_EVE = 2000, MOTOR_TERFOGAT = 123 }
            };

            repoMock.Setup(x => x.ReadAllModel()).Returns(table.AsQueryable());

            var logic = new Logic(repoMock.Object);

            logic.RemoveModellById(2);

            repoMock.Verify(x => x.RemoveModel(It.IsAny<MODELLEK>()), Times.Once());
        }

        /// <summary>
        /// Checks if the repository functions is called
        /// </summary>
        [Test]
        public void WhenRemoveModelByIdIsCalledWithInvalidId_ThrowsRecordDoesntExistException()
        {
            Mock<IRepository> repoMock = new Mock<IRepository>();

            List<MODELLEK> table = new List<MODELLEK>()
            {
                new MODELLEK { ID = 1, NEV = "tesztnev", ALAPAR = 123, LOERO = 123, MARKA_ID = 1, MEGJELENES_EVE = 2000, MOTOR_TERFOGAT = 123 },
                new MODELLEK { ID = 2, NEV = "tesztne2v", ALAPAR = 123, LOERO = 123, MARKA_ID = 1, MEGJELENES_EVE = 2000, MOTOR_TERFOGAT = 123 }
            };

            repoMock.Setup(x => x.ReadAllModel()).Returns(table.AsQueryable());

            var logic = new Logic(repoMock.Object);

            Assert.Throws<RecordDoesntExistException>(() => logic.RemoveModellById(3));
        }

        /// <summary>
        /// Checks if the repository functions is called
        /// </summary>
        [Test]
        public void WhenRemoveExtraByIdIsCalledWithExistingId_RemoveExtraIsCalledToo()
        {
            Mock<IRepository> repoMock = new Mock<IRepository>();

            List<EXTRAK> table = new List<EXTRAK>()
            {
                new EXTRAK() { ID = 1, NEV = "tesztnev", KATEGORIA_NEV = "tesztkategoria", AR = 123, IGENYTELEN_E = true, SZIN = "tesztszin" }
            };

            repoMock.Setup(x => x.ReadAllExtra()).Returns(table.AsQueryable());

            var logic = new Logic(repoMock.Object);

            logic.RemoveExtraById(1);

            repoMock.Verify(x => x.RemoveExtra(It.IsAny<EXTRAK>()), Times.Once());
        }

        /// <summary>
        /// Checks if the repository functions is called
        /// </summary>
        [Test]
        public void WhenRemoveExtraByIdIsCalledWithInvalidId_ThrowsRecordDoesntExistException()
        {
            Mock<IRepository> repoMock = new Mock<IRepository>();

            List<EXTRAK> table = new List<EXTRAK>()
            {
                new EXTRAK() { ID = 1, NEV = "tesztnev", KATEGORIA_NEV = "tesztkategoria", AR = 123, IGENYTELEN_E = true, SZIN = "tesztszin" }
            };

            repoMock.Setup(x => x.ReadAllExtra()).Returns(table.AsQueryable());

            var logic = new Logic(repoMock.Object);

            Assert.Throws<RecordDoesntExistException>(() => logic.RemoveExtraById(3));
        }

        /// <summary>
        /// Checks if the repository functions is called
        /// </summary>
        [Test]
        public void WhenRemoveExtraConnectionByIdIsCalledWithExistingId_RemoveExtraConnectionIsCalledToo()
        {
            Mock<IRepository> repoMock = new Mock<IRepository>();

            List<MODELL_EXTRA_KAPCSOLO> table = new List<MODELL_EXTRA_KAPCSOLO>()
            {
                new MODELL_EXTRA_KAPCSOLO { ID = 1, EXTRA_ID = 2, MODELL_ID = 3 }
            };

            repoMock.Setup(x => x.ReadAllExtraConnection()).Returns(table.AsQueryable());

            var logic = new Logic(repoMock.Object);

            logic.RemoveExtraConnectionById(1);

            repoMock.Verify(x => x.RemoveExtraConnection(It.IsAny<MODELL_EXTRA_KAPCSOLO>()), Times.Once());
        }

        /// <summary>
        /// Checks if the repository functions is called
        /// </summary>
        [Test]
        public void WhenRemoveExtraConnectionByIdIsCalledWithInvalidId_ThrowsRecordDoesntExistException()
        {
            Mock<IRepository> repoMock = new Mock<IRepository>();

            List<MODELL_EXTRA_KAPCSOLO> table = new List<MODELL_EXTRA_KAPCSOLO>()
            {
                new MODELL_EXTRA_KAPCSOLO { ID = 1, EXTRA_ID = 2, MODELL_ID = 3 }
            };

            repoMock.Setup(x => x.ReadAllExtraConnection()).Returns(table.AsQueryable());

            var logic = new Logic(repoMock.Object);

            Assert.Throws<RecordDoesntExistException>(() => logic.RemoveExtraConnectionById(3));
        }

        /// <summary>
        /// Checks if the repository functions is called
        /// </summary>
        [Test]
        public void WhenModifyBrandByIdIsCalled_ItsRepoMethodsAreCalledToo()
        {
            Mock<IRepository> repoMock = new Mock<IRepository>();

            List<AUTOMARKAK> table = new List<AUTOMARKAK>()
            {
                new AUTOMARKAK() { ID = 1, NEV = "tesztnev", ALAPITAS_EVE = 1997, EVES_FORGALOM = 1, C_URL = "teszturl", ORSZAGNEV = "tesztország" },
                new AUTOMARKAK() { ID = 2, NEV = "tesztnev2", ALAPITAS_EVE = 19927, EVES_FORGALOM = 12, C_URL = "teszturl2", ORSZAGNEV = "tesztország2" }
            };

            repoMock.Setup(x => x.ReadAllBrand()).Returns(table.AsQueryable());

            var logic = new Logic(repoMock.Object);
            logic.ModifyBrandById(1, "nev", "teszt");
            repoMock.Verify(x => x.ModifyBrand(It.IsAny<object>(), It.IsAny<string>(), It.IsAny<string>()), Times.Once);
        }

        /// <summary>
        /// Checks if the repository functions is called
        /// </summary>
        [Test]
        public void WhenModifModelByIdIsCalled_ItsRepoMethodsAreCalledToo()
        {
            Mock<IRepository> repoMock = new Mock<IRepository>();

            List<MODELLEK> table = new List<MODELLEK>()
            {
                new MODELLEK { ID = 1, NEV = "tesztnev", ALAPAR = 123, LOERO = 123, MARKA_ID = 1, MEGJELENES_EVE = 2000, MOTOR_TERFOGAT = 123 },
                new MODELLEK { ID = 2, NEV = "tesztne2v", ALAPAR = 123, LOERO = 123, MARKA_ID = 1, MEGJELENES_EVE = 2000, MOTOR_TERFOGAT = 123 }
            };

            repoMock.Setup(x => x.ReadAllModel()).Returns(table.AsQueryable());

            var logic = new Logic(repoMock.Object);
            logic.ModifyModellById(1, "nev", "teszt");
            repoMock.Verify(x => x.ModifyModel(It.IsAny<object>(), It.IsAny<string>(), It.IsAny<string>()), Times.Once);
        }

        /// <summary>
        /// Checks if the repository functions is called
        /// </summary>
        [Test]
        public void WhenModifyExtraByIdIsCalled_ItsRepoMethodsAreCalledToo()
        {
            Mock<IRepository> repoMock = new Mock<IRepository>();

            List<EXTRAK> table = new List<EXTRAK>()
            {
                new EXTRAK() { ID = 1, NEV = "tesztnev", KATEGORIA_NEV = "tesztkategoria", AR = 123, IGENYTELEN_E = true, SZIN = "tesztszin" }
            };

            repoMock.Setup(x => x.ReadAllExtra()).Returns(table.AsQueryable());

            var logic = new Logic(repoMock.Object);
            logic.ModifyExtraById(1, "nev", "teszt");
            repoMock.Verify(x => x.ModifyExtra(It.IsAny<object>(), It.IsAny<string>(), It.IsAny<string>()), Times.Once);
        }
    }
}
