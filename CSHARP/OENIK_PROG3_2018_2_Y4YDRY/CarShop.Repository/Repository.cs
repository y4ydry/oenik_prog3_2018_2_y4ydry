﻿// <copyright file="Repository.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace CarShop.Repo
{
    using System.Linq;
    using System.Reflection;
    using Data;

    /// <summary>
    /// Repository for CarShop
    /// </summary>
    public sealed class Repository : IRepository, System.IDisposable
    {
        private readonly CarShopDataEntities context;

        /// <summary>
        /// Initializes a new instance of the <see cref="Repository"/> class.
        /// Empty constructor. Creates a new dbContext for itself
        /// </summary>
        public Repository()
        {
            this.context = new CarShopDataEntities();
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="Repository"/> class.
        /// Constructor, that sets the context, to the one given to the constructor
        /// </summary>
        /// <param name="context">database model</param>
        public Repository(CarShopDataEntities context)
        {
            this.context = context;
        }

        /// <summary>
        /// Function to insert new brand.
        /// </summary>
        /// <param name="newItem">instance to be inserted</param>
        public void InsertBrand(AUTOMARKAK newItem)
        {
            this.context.AUTOMARKAK.Add(newItem);
            this.context.SaveChanges();
        }

        /// <summary>
        /// Function to insert new extra.
        /// </summary>
        /// <param name="newItem">instance to be inserted</param>
        public void InsertExtra(EXTRAK newItem)
        {
            this.context.EXTRAK.Add(newItem);
            this.context.SaveChanges();
        }

        /// <summary>
        /// Function to insert new model extra connection.
        /// </summary>
        /// <param name="newItem">instance to be inserted</param>
        public void InsertExtraConnection(MODELL_EXTRA_KAPCSOLO newItem)
        {
            this.context.MODELL_EXTRA_KAPCSOLO.Add(newItem);
            this.context.SaveChanges();
        }

        /// <summary>
        /// Function to insert new model.
        /// </summary>
        /// <param name="newItem">instance to be inserted</param>
        public void InsertModel(MODELLEK newItem)
        {
            this.context.MODELLEK.Add(newItem);
            this.context.SaveChanges();
        }

        /// <summary>
        /// Function for listing  brands.
        /// </summary>
        /// <returns> a quearyable list</returns>
        public IQueryable<AUTOMARKAK> ReadAllBrand()
        {
            return this.context.AUTOMARKAK;
        }

        /// <summary>
        /// Function for listing  extras.
        /// </summary>
        /// <returns> a quearyable list</returns>
        public IQueryable<EXTRAK> ReadAllExtra()
        {
            return this.context.EXTRAK.AsQueryable();
        }

        /// <summary>
        /// Function for listing  model extra connection.
        /// </summary>
        /// <returns> a quearyable list</returns>
        public IQueryable<MODELL_EXTRA_KAPCSOLO> ReadAllExtraConnection()
        {
            return this.context.MODELL_EXTRA_KAPCSOLO.AsQueryable();
        }

        /// <summary>
        /// Function for listing  models.
        /// </summary>
        /// <returns> a quearyable list</returns>
        public IQueryable<MODELLEK> ReadAllModel()
        {
            return this.context.MODELLEK.AsQueryable();
        }

        /// <summary>
        /// Function for modifying brands. Record is selected by primary key
        /// </summary>
        /// <typeparam name="T">Type of primary key</typeparam>
        /// <param name="pk">Primary key of record</param>
        /// <param name="columnName">Column to be modified</param>
        /// <param name="changeTo">Value to change to</param>
        public void ModifyBrand<T>(T pk, string columnName, string changeTo)
        {
            var q = this.context.AUTOMARKAK.Find(pk);
            switch (columnName.ToLower())
            {
                case "nev":
                    q.NEV = changeTo;
                    break;
                case "id":
                    q.ID = decimal.Parse(changeTo);
                    break;
                case "orszagnev":
                    q.ORSZAGNEV = changeTo;
                    break;
                case "c_url":
                    q.C_URL = changeTo;
                    break;
                case "alapitas_eve":
                    q.ALAPITAS_EVE = decimal.Parse(changeTo);
                    break;
                case "eves_forgalom":
                    q.EVES_FORGALOM = decimal.Parse(changeTo);
                    break;
            }

            this.context.SaveChanges();
        }

        /// <summary>
        /// Function for modifying extras. Record is selected by primary key
        /// </summary>
        /// <typeparam name="T">Type of primary key</typeparam>
        /// <param name="pk">Primary key of record</param>
        /// <param name="columnName">Column to be modified</param>
        /// <param name="changeTo">Value to change to</param>
        public void ModifyExtra<T>(T pk, string columnName, string changeTo)
        {
            var q = this.context.EXTRAK.Find(pk);
            switch (columnName.ToLower())
            {
                case "id":
                    q.ID = decimal.Parse(changeTo);
                    break;
                case "nev":
                    q.NEV = changeTo;
                    break;
                case "kategoria_nev":
                    q.KATEGORIA_NEV = changeTo;
                    break;
                case "ar":
                    q.AR = decimal.Parse(changeTo);
                    break;
                case "szin":
                    q.SZIN = changeTo;
                    break;
                case "igenytelen_e":
                    if (changeTo == "0")
                    {
                        q.IGENYTELEN_E = false;
                    }
                    else
                    {
                        q.IGENYTELEN_E = true;
                    }

                    break;
            }

            this.context.SaveChanges();
        }

        /// <summary>
        /// Function for modifying models. Record is selected by primary key
        /// </summary>
        /// <typeparam name="T">Type of primary key</typeparam>
        /// <param name="pk">Primary key of record</param>
        /// <param name="columnName">Column to be modified</param>
        /// <param name="changeTo">Value to change to</param>
        public void ModifyModel<T>(T pk, string columnName, string changeTo)
        {
            var q = this.context.MODELLEK.Find(pk);
            switch (columnName.ToLower())
            {
                case "id":
                    q.ID = decimal.Parse(changeTo);
                    break;
                case "marka_id":
                    q.MARKA_ID = decimal.Parse(changeTo);
                    break;
                case "nev":
                    q.NEV = changeTo;
                    break;
                case "megjelenes_eve":
                    q.MEGJELENES_EVE = decimal.Parse(changeTo);
                    break;
                case "motor_terfogat":
                    q.MOTOR_TERFOGAT = decimal.Parse(changeTo);
                    break;
                case "loero":
                    q.LOERO = decimal.Parse(changeTo);
                    break;
                case "alapar":
                    q.ALAPAR = decimal.Parse(changeTo);
                    break;
            }

            this.context.SaveChanges();
        }

        /// <summary>
        /// Function for removing brands
        /// </summary>
        /// <param name="itemToBeDeleted">Instance of record to be removed</param>
        public void RemoveBrand(AUTOMARKAK itemToBeDeleted)
        {
            this.context.AUTOMARKAK.Remove(itemToBeDeleted);
            this.context.SaveChanges();
        }

        /// <summary>
        /// Function for removing extras.
        /// </summary>
        /// <param name="itemToBeDeleted">Instance of record to be removed</param>
        public void RemoveExtra(EXTRAK itemToBeDeleted)
        {
            this.context.EXTRAK.Remove(itemToBeDeleted);
            this.context.SaveChanges();
        }

        /// <summary>
        /// Function for removing model extra connections.
        /// </summary>
        /// <param name="itemToBeDeleted">Instance of record to be removed</param>
        public void RemoveExtraConnection(MODELL_EXTRA_KAPCSOLO itemToBeDeleted)
        {
            this.context.MODELL_EXTRA_KAPCSOLO.Remove(itemToBeDeleted);
            this.context.SaveChanges();
        }

        /// <summary>
        /// Function for removing models
        /// </summary>
        /// <param name="itemToBeDeleted">Instance of record to be removed</param>
        public void RemoveModel(MODELLEK itemToBeDeleted)
        {
            this.context.MODELLEK.Remove(itemToBeDeleted);
            this.context.SaveChanges();
        }

        /// <summary>
        /// disposes of dbcontext
        /// </summary>
        public void Dispose()
        {
            this.context.Dispose();
        }
    }
}
