﻿// <copyright file="IRepository.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace CarShop.Repo
{
    using System.Linq;
    using Data;

    /// <summary>
    /// functions for CarShop repository
    /// </summary>
    public interface IRepository
    {
        /// <summary>
        /// Function to insert new brand.
        /// </summary>
        /// <param name="newItem">instance to be inserted</param>
        void InsertBrand(AUTOMARKAK newItem);

        /// <summary>
        /// Function to insert new model.
        /// </summary>
        /// <param name="newItem">instance to be inserted</param>
        void InsertModel(MODELLEK newItem);

        /// <summary>
        /// Function to insert new extra.
        /// </summary>
        /// <param name="newItem">instance to be inserted</param>
        void InsertExtra(EXTRAK newItem);

        /// <summary>
        /// Function to insert new model extra connection.
        /// </summary>
        /// <param name="newItem">instance to be inserted</param>
        void InsertExtraConnection(MODELL_EXTRA_KAPCSOLO newItem);

        /// <summary>
        /// Function for removing  brands.
        /// </summary>
        /// <param name="itemToBeDeleted">instance to be removed</param>
        void RemoveBrand(AUTOMARKAK itemToBeDeleted);

        /// <summary>
        /// Function for removing  models.
        /// </summary>
        /// <param name="itemToBeDeleted">instance to be removed</param>
        void RemoveModel(MODELLEK itemToBeDeleted);

        /// <summary>
        /// Function for removing  extras.
        /// </summary>
        /// <param name="itemToBeDeleted">instance to be removed</param>
        void RemoveExtra(EXTRAK itemToBeDeleted);

        /// <summary>
        /// Function for removing  model extra connection.
        /// </summary>
        /// <param name="itemToBeDeleted">instance to be removed</param>
        void RemoveExtraConnection(MODELL_EXTRA_KAPCSOLO itemToBeDeleted);

        /// <summary>
        /// Function for modifying brands.
        /// </summary>
        /// <typeparam name="T">Type of primary key</typeparam>
        /// <param name="pk">Value of primary key</param>
        /// <param name="columnName">Column name to be modified</param>
        /// <param name="changeTo">New value</param>
        void ModifyBrand<T>(T pk, string columnName, string changeTo);

        /// <summary>
        /// Function for modifying models.
        /// </summary>
        /// <typeparam name="T">Type of primary key</typeparam>
        /// <param name="pk">Value of primary key</param>
        /// <param name="columnName">Column name to be modified</param>
        /// <param name="changeTo">New value</param>
        void ModifyModel<T>(T pk, string columnName, string changeTo);

        /// <summary>
        /// Function for modifying extras.
        /// </summary>
        /// <typeparam name="T">Type of primary key</typeparam>
        /// <param name="pk">Value of primary key</param>
        /// <param name="columnName">Column name to be modified</param>
        /// <param name="changeTo">New value</param>
        void ModifyExtra<T>(T pk, string columnName, string changeTo);

        /// <summary>
        /// Function for listing brands.
        /// </summary>
        /// <returns> a quearyable list</returns>
        IQueryable<AUTOMARKAK> ReadAllBrand();

        /// <summary>
        /// Function for listing models.
        /// </summary>
        /// <returns> a quearyable list</returns>
        IQueryable<MODELLEK> ReadAllModel();

        /// <summary>
        /// Function for listing extras.
        /// </summary>
        /// <returns> a quearyable list</returns>
        IQueryable<EXTRAK> ReadAllExtra();

        /// <summary>
        /// Function for listing  model extra connection.
        /// </summary>
        /// <returns> a quearyable list</returns>
        IQueryable<MODELL_EXTRA_KAPCSOLO> ReadAllExtraConnection();
    }
}
